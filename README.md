check\_ftp
==========

This plugin makes an FTP connection to a nominated server, optionally including
a STARTTLS upgrade for FTPS.  It does not support implicit FTPS.

Installation
------------

    $ perl Makefile.PL
    $ make
    $ sudo make install

Usage
-----

    Usage: check_ftp [--hostname|h HOSTNAME] [--starttls|S [SERVERNAME]]

     -?, --usage
       Print usage information
     -h, --help
       Print detailed help screen
     -V, --version
       Print version information
     --extra-opts=[section][@file]
       Read options from an ini file. See https://www.monitoring-plugins.org/doc/extra-opts.html
       for usage and examples.
     -H, --hostname=HOSTNAME
       FTP server hostname or address
     -S, --starttls[=HOSTNAME]
       Try STARTTLS with optional specified hostname
     -t, --timeout=INTEGER
       Seconds before plugin times out (default: 15)
     -v, --verbose
       Show details for command-line debugging (can repeat up to 3 times)


Thanks
------

This was written on company time with my employer [Inspire Net][1], who has
generously allowed me to release it as free software.

License
-------

Copyright (C) 2020--2021 Tom Ryder <tom@sanctum.geek.nz>

Distributed under GNU General Public License version 3 or any later version.
Please see `COPYING`.

[1]: https://www.inspire.net.nz/
